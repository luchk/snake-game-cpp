#include <iostream>
#include <iostream>
#include <cstdio>

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
using namespace std;
#define L 10
#define H 15

int map[H][L];
// int snake[];
// int snakemap[15][25];
int head[2] = {5, 5};
int food_coo[2];


char dir;
int direction = 0;

int getch();
int kbhit();
void printMap();
void up(int direction);

void read();
void food();

int main()
{
  food();
  while (1) {
    system("clear");
    printMap();
    cout << direction << '\n'<<endl;
    cout << "food x " <<food_coo[0] << "food y" << food_coo[1] << '\n'<<endl;
    // for (size_t i = 0; i < 100; i++) {
    //   std::cout << 1+rand() %  L -1<< '\n';
    // }
    read();
    up(direction);
    if (head[0] == food_coo[0] && head[1] == food_coo[1]) {
      food();
    }

    // std::cout << "food x " <<food_coo[0] << "food y" << food_coo[1] << '\n';
  }
  return 0;
}





int getch()
{
    struct termios oldt, newt;
    int  ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = std::getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    // std::putchar(ch);
    return ch;
}

int kbhit()
{
	struct timeval tv;
	fd_set fds;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(STDIN_FILENO, &fds);
	select(STDIN_FILENO+1 , &fds, NULL, NULL, &tv);
	return FD_ISSET(STDIN_FILENO, &fds);
}

void printMap()
{
  for (int i = 0; i < H; i++) {
    for (int k = 0; k < L; k++) {
      // (i == 0 || i == H-1 ) ? std::cout << "#" : ((k == 0 || k == L-1) ? std::cout << "#" : std::cout << " ");
      // (i == 0 || i == H-1 || i == head[0]) ? std::cout << "#" : ((k == 0 || k == L-1 || k == head[1]) ? std::cout << "#" : std::cout << " ");
      (i == 0 || i == H-1 ) ? map[i][k] = 1 : ((k == 0 || k == L-1) ? map[i][k] = 1 : map[i][k] = 0);
    }
    // std::cout << "\n";
  }
  map[head[0]][head[1]] = 1;
  int x_food=food_coo[0];
  int y_food=food_coo[1];
  map[x_food][y_food] = 2;
  for (int i = 0; i < H; i++) {
    for (int k = 0; k < L; k++) {
      // (i == 0 || i == H-1 ) ? std::cout << "#" : ((k == 0 || k == L-1) ? std::cout << "#" : std::cout << " ");
      // (i == 0 || i == H-1 || i == head[0]) ? std::cout << "#" : ((k == 0 || k == L-1 || k == head[1]) ? std::cout << "#" : std::cout << " ");
      // (i == 0 || i == H-1 ) ? map[i][k] = 1 : ((k == 0 || k == L-1) ? map[i][k] = 1 : map[i][k] = 0);
      if(map[i][k] == 1)
        cout << "#";
      if (map[i][k] == 0) {
        cout << " ";
      }
      if(map[i][k] == 2)
        cout << "0";
      if (map[i][k] == 0) {
        // std::cout << " ";
      }
    }
    cout << "\n";
  }
}

void read()
{
  char a = getch();
  switch (a)
  {
    case 'w':
      direction = 0;
    break;
    case 'd':
      direction = 1;
    break;
    case 'a':
      direction = 2;
    break;
    case 's':
      direction = 3;
    break;
    case 'Q':
      exit(0);
    break;
    default: break;
  }
}

void up(int direction)
{
  switch (direction)
  {
    case 0:
    head[0] -= 1;
    break;
    case 1:
    head[1] += 1;
    break;
    case 2:
    head[1] -= 1;
    break;
    case 3:
    head[0] += 1;
    break;
  }
}

void food()
{
  int a = H-2;
  int b = L-2;
  food_coo[0] = 1+rand() % a;
  food_coo[1] = 1+rand() % b;
}
